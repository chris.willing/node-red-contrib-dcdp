# node-red-contrib-xkeys_backlight

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices using the Device Control Data Protocol (DCDP).

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight [DCDP server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_backlight_ node encapsulates Backlight operations of enabling or disabling the Hue Cannels for any or specific X-keys devices.


## Installation

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_backlight_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-dcdp\_backlight_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk Backlight_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the target Xkeys device, as well as the target button(s) to operate on. Settings in the configuration editor may be overridden by settings the relevant field in the msg.payload of an incoming node. A msg.payload containing only an _action_ field activates backlights with whatever settings are currently configured in the configuration editor.
The msg.payload expected by the _xkey\_backlight_ node is as follows:
```
{ action: STARTSTOP, control_id: ID, flashing: YESNO, hue_channels: CHANNELS, pid_list: [e1,e2,..,eN] | pid: PID, unit_id:UID, duplicate_id:DUPID}
```
where
- STARTSTOP is either of *"start"* or *"stop"*. This is the only required field. All other fields are optional and, if not provided, the configured settings of the node are used.
- ID indicates which button(s) to operate on. Multiple buttons may be targeted by entering comma separated ID numbers in this field e.g. 1,3,6,7 whereas an empty buttonid value indicates that _all_ available backlights should be targeted.
- YESNO is one of _true_ or _false_
- CHANNELS is a string describing the which channels will be active e.g. ff0000 => red, 0000ff => blue. Not all devices are capable of all hues.
- PID is the Product Id of the device. Alternatively and array of device PIDs may be specified instead using the _pid_list_ field.
- UID is the Unit Id of the device.
- DUPID is the Duplicate Id of the device (allocated by the DCDP server to distinguish devices with same PID & UID).

An example flow is provided in the _examples_ directory to demonstrate activation of the _xkeys\_backlight_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_backlight-test.png" ></p>

- the first method is via the toggle switches attached to the node itself.
- a second method is to use _Inject_ nodes with appropriately crafted msg.payloads.

The lower example uses the second method so that an _xkeys\_button_ node activates the _xkeys\_backlight_ node by via a _function_ node (labeled _Button to Backlight_) which is configured to use the _button_ node's "down" and "up" actions to activate the red and blue channels respecitvely, as well as setting a pid_list derived from whichever physical device is activating the _button_ node.

Note the use of an _xkeys\_intensity_ node set the intensity of each channel. In this case the blue and red intensities are both set to maximum.

## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the xkeys-server by running the command `sudo systemctl status xkeys-server` in a terminal. Typical output of a normally running xkeys-server will be:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running dcdp-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the dcdp-server with `sudo systemctl restart xkeys-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several X-keys devices for development and testing.

## License
MIT
