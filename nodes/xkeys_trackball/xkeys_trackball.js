
module.exports = function(RED) {
	const XKEYS_VENDOR_ID = "1523";
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;	// Connection to local broker should be good

	var httpAdminDataProducts = {};
	var httpAdminDataDevices = {};
	
	function XkeysTrackball(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log("node.config = " + JSON.stringify(node.config));
		//node.log("myId = " + node.config.id);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			//node.log('connected')
			//client.subscribe('/dcdp/server/#', function (err, granted) {
			client.subscribe({'/dcdp/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/dcdp/node', JSON.stringify({msg_type:"all_product_data"}));
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log(`SID = ${message_obj.server_id}`);
				if (message_obj.mst_type == "hello") {
					console.log(`Hello from DCDP server at ${message_obj.server_id} - must have just (re)started `);
					// In case dcdp-server restarted with updated devices/product list
					client.publish('/dcdp/node', JSON.stringify({msg_type:"all_product_data"}));
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "list_attached_result") {
					// data should be a dict of info objects
					httpAdminDataDevices = [];
					message_obj.devices.forEach( (device) => {
						if (device.vendorId == XKEYS_VENDOR_ID) {
							httpAdminDataDevices.push(device);
						}
					});

					var pid_list = this.config.pid_list || "[]";
					if (device_connected(this.config.vendor_id || XKEYS_VENDOR_ID, JSON.parse(pid_list), this.config.unit_id, this.config.duplicate_id)) {
						node.status( {fill:"green",shape:"dot",text:"connected"} );
					} else {
						node.status( {fill:"red",shape:"ring",text:"disconnected"} );
					}
				}
				else if (message_obj.msg_type == "all_product_data_result") {
					// data should be a dict of product objects
					httpAdminDataProducts = {};
					// Restrict products to X-keys devices
					Object.keys(message_obj.data).forEach( (device) => {
						if (message_obj.data[device].vendorId == XKEYS_VENDOR_ID ) {
							httpAdminDataProducts[device] = message_obj.data[device];
						}
					});
				}
				else if (message_obj.request == "trackball_event") {
					// Check that PID & UID are what we're interested in
					var pid_list = this.config.pid_list || "[]";
					var pids = JSON.parse(pid_list);
					if (pids.length == 0 || pids.includes(parseInt(message_obj.product_id))) {
						// Either no PID was configured i.e. any PID will do
						// or this event is for a PID that has been configured

						// Now filter Controller #
						if (message_obj.control_id == this.config.control_id || this.config.control_id == "") {
							// Either this event was for a configured control_id or no control_id was configured

							// Now filter Unit #
							if (message_obj.unit_id == this.config.unit_id || this.config.unit_id == "") {
								// Either event was for a configured unit_id or no unit_id was configured

								// Now filter Duplicate #
								if (message_obj.duplicate_id == this.config.duplicate_id || this.config.duplicate_id == "") {
									// Either event was for a configured duplicate_id or no duplicate_id was configured

									// Prepare output msg
									var msg = {
										device       : message_obj.device,
										vendor_id    : message_obj.vendor_id,
										product_id   : message_obj.product_id,
										unit_id      : message_obj.unit_id,
										duplicate_id : message_obj.duplicate_id,
										control_id   : message_obj.control_id,
										x            : message_obj.x,
										y            : message_obj.y
									};
									node.send(msg);
								}
							}
						}
					}
				}
				else {
					// Logging here may be useful but is quietened for production
					//node.log(`Received unhandled request: ${message_obj.msg_type}`);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		// Removed for production
	    // Input messages
		//node.on('input', function(msg) {
		//	if (msg.payload == "close") {
		//		console.log("close requested");
		//		client.end();
		//	} else if (msg.payload == "refresh") {
		//		console.log("Refresh requested");
		//		client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
		//	}
		//})

		this.on('close', function(done) {
			client.end();
			done();
		})

		// Does any attached device match specified pids, unit_id & dup_id ?
		// pids: array of possible PIDs for a device (empty => ANY)
		// unit_id: unitId of a device
		// dup_id:  duplicate_id of a device
		function device_connected(...deviceArgs) {
			const vendor_id = deviceArgs[0];
			const pids = deviceArgs[1];
			const unit_id = deviceArgs[2];
			const dup_id = deviceArgs[3];
			var device_matched = false;
			var regex_string = ""
			var regex;

			if (pids.length == 0) {
				if (vendor_id) {
					regex_string = vendor_id + "-";
				} else {
					regex_string = "\[0-9\]+-";
				}
				//	No product_ids provided => ANY product_id
				regex_string = regex_string + "\[0-9\]+-";
				if (unit_id) {
					regex_string = regex_string + unit_id + "-";
				} else {
					regex_string = regex_string + "\[0-9\]+-";
				}
				if (dup_id) {
					regex_string = regex_string + dup_id;
				} else {
					regex_string = regex_string + "\[0-9\]+";
				}
				regex = new RegExp(regex_string);
				httpAdminDataDevices.forEach( (item) => {
					if (regex.test(item.device_quad)) { device_matched = true; }
				});
			} else {
				//	An array of endpoints provided
				pids.forEach(function (item) { 
					if (vendor_id) {
						regex_string = vendor_id + "-";
					} else {
						regex_string = "\[0-9\]+-";
					}
					regex_string = regex_string + item + "-";
					if (unit_id) {
						regex_string = regex_string + unit_id + "-";
					} else {
						regex_string = regex_string + "\[0-9\]+-";
					}
					if (dup_id) {
						regex_string = regex_string + dup_id;
					} else {
						regex_string = regex_string + "\[0-9\]+";
					}
					regex = new RegExp(regex_string);
					httpAdminDataDevices.forEach( (item) => {
						if (regex.test(item.device_quad)) { device_matched = true; }
					})
				})
			}
			return device_matched;

		}	// function device_connected

	}	// function XkeysTrackball

	RED.nodes.registerType("xkeys_trackball", XkeysTrackball);

	RED.httpAdmin.get("/xkeys_trackball/products", function (req, res) {
		res.json(httpAdminDataProducts);
	});
	RED.httpAdmin.get("/xkeys_trackball/devices", function (req, res) {
		res.json(httpAdminDataDevices);
	});

}
/*
	Node colours:
	Light blue is: R:  93, G: 200, B: 244 (#5dc8f4)
	Dark blue is:  R:  78, G: 169, B: 242 (#4ea8f2)
*/

