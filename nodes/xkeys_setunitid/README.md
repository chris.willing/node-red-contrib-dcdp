# node-red-contrib-xkeys_setunitid

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices using Dynamic Control Data Protocol (DCDP).

A dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight [DCDP server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

Most X_keys devices ship with a default Unit ID of 0. This _xkeys\_setunitid_ node enables specific X-keys devices to change the Unit ID.


## Installation

Please note that this _xkeys\_setunitid_ node runs correctly only on version 4 of the Raspberry Pi - USB hardware on previous versions is insufficient for this node to run cleanly. Success on ordinary computers depends on the age of the USB hardware being used.

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_setunitid_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_setunitid_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk Set Unit ID_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor must be used to explicitly set the target X-keys device, as well as both current and proposed new Unit IDs of the device. The node's built in button can then be used to activate the change of Unit ID, at which time the targeted device will be rediscovered with its new Unit ID.

An example flow is provided in the _examples_ directory to demonstrate how the _xkeys\_setunitid_ node could be used.
<p align="center" width="100%" ><img width="50%" src="examples/xkeys_setunitid-test.png" ></p>

In this example, a Debug node showing the msg.payload of an _xkeys\_button_ enables accurate discovery of the current Unit ID of the target device. This, along with the new Unit ID, needs to be set in the _xkeys\_setunitid_ configuration editor. When an attached device, Unit ID and new Unit ID have been configured correctly and deployed, the node will display a button.  Clicking the _xkeys\_setunitid_ button executes the change of the device Unit ID.

On modern hardware e.g. RPI4, the change will be discovered automatically resulting in the _xkeys\_setunitid_ node's status changing to _disconnected_ (since it is configured with the device's _old_ Unit ID). On older hardware, the device may need to be replugged for the change in Unit ID to be discvovered.


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the dcdp-server by running the command `sudo systemctl status dcdp-server` in a terminal. Typical output of a normally running dcdp-server will be:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running dcdp-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the dcdp-server with `sudo systemctl restart dcdp-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several X-keys devices for development and testing.

## License
MIT
