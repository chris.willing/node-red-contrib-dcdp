
module.exports = function(RED) {
	const XKEYS_VENDOR_ID = "1523";
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;

	var httpAdminDataDevices = [];
	var httpAdminDataPidOptions = {};
	var httpAdminDataQuadList = [];
	var httpAdminDataSetUnitID = {};
	
	function XkeysSetUnitID(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log(`node.config = ${JSON.stringify(node.config)}`);
		//node.log(`myId = ${node.config.id}`);
		//node.log(`pidOptions = ${JSON.stringify(node.config.pidOptions}}`);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe({'/dcdp/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log(`Subscribed OK, granted: ${JSON.stringify(granted)}`);
					client.publish('/dcdp/node/xkeys_setunit', '{"msg_type":"list_attached"}')
				} else {
					node.log('Subscription failed: ' + err)
				}
			});
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log(`SID = ${message_obj.server_id}`);
				if (message_obj.msg_type == "hello") {
					console.log(`Hello from DCDP server at ${message_obj.sid} - must have just (re)started `);
					// In case dcdp-server restarted with updated devices list
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "attach_event") {
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "detach_event") {
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "command_result") {
					if ( message_obj.command_type == "set_unit_id") {
						// A unit_id was changed so update device list
						client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
					}
				}
				else if (message_obj.msg_type == "list_attached_result") {
					// data should be an array of device infos
					httpAdminDataDevices = [];

					// Collect only X-keys devices
					message_obj.devices.forEach( (device) => {
						if (device.vendorId == XKEYS_VENDOR_ID) {
							httpAdminDataDevices.push(device);
						}
					});

					/* Check whether a previously configured device has been removed.
					*/
					for (var i=node.config.quad_list.length-1;i>-1;i--) {
						//console.log(`checking ${node.config.quad_list[i]}`);
						if (! httpAdminDataDevices.find( (device) => device.device_quad == node.config.quad_list[i])) {
							// This device was removed so remove it from both
							// node.config.pidOptions and node.config.quad_list
							var index = node.config.pidOptions.findIndex( (opt) => opt.device_quad == node.config.quad_list[i]);
							if (index > -1 ) {
								node.config.pidOptions[index].checked = false;
								node.config.pidOptions.splice(index, 1);
							}
							node.config.quad_list.splice(i,1);
						}
					}
					httpAdminDataPidOptions = node.config.pidOptions;
					httpAdminDataQuadList = node.config.quad_list;

					// Update status (how many configured devices)
					if (node.config.quad_list.length == 0) {
						node.status( {fill:"red",shape:"ring",text:`No devices configured`} );
					}
					else if (node.config.quad_list.length == 1) {
						node.status( {fill:"green",shape:"dot",text:`${node.config.quad_list.length} device configured`} );
					} else {
						node.status( {fill:"green",shape:"dot",text:`${node.config.quad_list.length} devices configured`} );
					}

				}
				else {
					// Logging here may be useful but is quietened for production
					//node.log(`Received unhandled request: ${message_obj.msg_type}`);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect a msg.payloads of
		*
		*/
		node.on('input', function(msg) {
			//console.log("INPUT: " + JSON.stringify(msg));
			var mkeys = Object.keys(msg.payload);
			//console.log("mkeys: " + JSON.stringify(mkeys));
			//console.log("msg.payload: " + JSON.stringify(msg.payload));
			if (mkeys.includes("new_unit_id")) {
				/*
				*   Check that the request is OK. In particular,
				*   ensure that target UID is not already used by another device with same VID & PID.
				*
				*   For "internal" injected commands, this has already been checked in the config editor.
				*   However we still need to check inputs from prior nodes in a flow
				*	(if we allow prior node input - which we do not at the moment).
				*/
				//console.log(`new_unit_id = ${msg.payload.new_unit_id}`);
				//console.log(`new_unit_id length = ${msg.payload.new_unit_id.length}`);
				//console.log(`typeof new_unit_id = ${typeof(msg.payload.new_unit_id)}`);
				//console.log(`new_unit_id (int) = ${parseInt(msg.payload.new_unit_id)}`);

				/*  Be careful!
				*	"fx30" becomes isNaN
				*
				*	"30fx" becomes 30
				*/
				const new_unit_id = parseInt(msg.payload.new_unit_id);
				if (isNaN(new_unit_id)) {
					if (msg.payload.new_unit_id.length == 0) {
						// Empty string
						node.log(`attempt to set new_unit_id to an empty string`);
						return;
					} else {
						// An actual string - we may support that sometime soon ...
						node.log(`new_unit_id is set to a string (${msg.payload.new_unit_id})`);
						return;
					}
				}
				if ((new_unit_id < 0) || (new_unit_id > 255)) {
					node.log(`attempt to set new_unit_id out of range (${new_unit_id})`);
					return;
				}

				/*  Process incoming requests for particular device(s)
				*/
				var quad_list = [];
				var new_quad_list_needed = false;
				var quad_list_regex_string = XKEYS_VENDOR_ID + "-";
				var target_unitid_regex_string = XKEYS_VENDOR_ID + "-";
				if (mkeys.includes("product_id")) {
					new_quad_list_needed = true;
					quad_list_regex_string += msg.payload.product_id + "-";
					target_unitid_regex_string += msg.payload.product_id + "-";
				} else {
					quad_list_regex_string += "\[0-9\]+-";
					target_unitid_regex_string += "\[0-9\]+-";
				}
				if (mkeys.includes("unit_id")) {
					new_quad_list_needed = true;
					quad_list_regex_string += msg.payload.unit_id + "-";
				} else {
					quad_list_regex_string += "\[0-9\]+-";
				}
				target_unitid_regex_string += new_unit_id + "-";
				if (mkeys.includes("duplicate_id")) {
					new_quad_list_needed = true;
					quad_list_regex_string += msg.payload.duplicate_id;
					target_unitid_regex_string += msg.payload.duplicate_id;
				} else {
					quad_list_regex_string += "\[0-9\]+";
					target_unitid_regex_string += "\[0-9\]+";
				}

				/* 1. If any of product_id, unit_id or duplicate_id was requested,
				*  add any matches to 'quad_list' array.
				*
				*  2. An alternative to adding to the node's default quad_list
				*  would be to restrict to devices already in node's default quad_list.
				*
				*  3. Another alternative would be to match against any attached device.
				*
				*  Here, we're using option 2. Since the config editor restricts the
				*  default quad_list to a single device, the effect is that incoming
				*  device requests must resolve to what is already configured.
				*/
				//if (new_quad_list_needed) {
					// Incoming message requested at least one of product_id, unit_id or duplicate_id
					//console.log(`quad_list_regex_string = ${quad_list_regex_string}`);
					var regex = new RegExp(quad_list_regex_string);
					node.config.quad_list.forEach( (quad) => {
						//console.log(`checking ${quad} against ${quad_list_regex_string}`);
						if (regex.test(quad)) {
							quad_list.push(quad);
						}
					});
					//console.log(`quad_list = ${JSON.stringify(quad_list)}`);
				//}

				/*  Also need to check that target unit_id isn't taken by any attached device
				*   i.e. check target_unitid_regex_string against _all_ attached devices
				*   (not just from configured quad_list)
				*/
				//console.log(`target_unitid_regex_string = ${target_unitid_regex_string}`);
				var regex = new RegExp(target_unitid_regex_string);
				var target_unitid_used = false;
				var target_unitid_used_by = "";
				node.config.pidOptions.every( (quad) => {
					//console.log(`checking ${quad.device_quad} against ${target_unitid_regex_string}`);
					if (regex.test(quad.device_quad)) {
						target_unitid_used = true
						target_unitid_used_by = quad.device_quad;
						return false;
					}
					return true;
				});
				if (target_unitid_used) {
					node.log(`requested unit_id (${new_unit_id}) already used by ${target_unitid_used_by} so discontinuing`);
					return;
				}

				/*
				*  If no quad_list is set by anyone, discontinue.
				*/
				if (quad_list.length == 0) {
					if (new_quad_list_needed) {
						// Particular device(s) requested but nothing matched, so discontinue
						node.log(`No devices matched: discontinuing`);
						return;
					} else {
						// Nothing in particular requested so use devices enabled in configuration
						quad_list = node.config.quad_list;
					}
				}
				if (quad_list.length == 0 ) {
					node.log(`No devices enabled: discontinuing`);
					return;
				}


				/*  Prepare output msg but only bother if we have a device with matching device_quad.
				*/
				httpAdminDataDevices.every( (device) => {
					if (quad_list.includes(device.device_quad)) {
						const command_message = {
							msg_type     : "command",
							command_type : "set_unit_id",
							vendor_id    : XKEYS_VENDOR_ID,
							product_id   : device.device_quad.split('-')[1],
							unit_id      : device.device_quad.split('-')[2],
							duplicate_id : device.device_quad.split('-')[3],
							new_unit_id  : new_unit_id
						}
						//console.log(`command_message = ${JSON.stringify(command_message)}`);
						client.publish('/dcdp/node', JSON.stringify(command_message));

						// Don't break the every loop on first match - find all matches.
						//return false;
					}
					return true;
				});

				// Ask server to change the Unit ID
				/*
				*	Message format:
				*	{request: "method", pid_list:[e0,e1,e2,...,eN], uid:UID, name:METHODNAME, params: [p0,p1,...,pN]}
				*	where p0 is an empty, not used, array in this case
				var method_request = {	"request"  : "method",
									"pid_list"     : JSON.parse(msg.payload.pid_list),
									"unit_id"      : unit_id.length==0?-1:parseInt(unit_id),
									"duplicate_id" : duplicate_id.length==0?-1:parseInt(duplicate_id),
									"name"         : "setUnitID",
									"params"       : [[], msg.payload.new_unit_id]
								} || {};
				client.publish('/xkeys/node/xkeys_setunit_id', JSON.stringify(method_request));
				*/

			} else {
				console.log(`Not interested in this msg (no \"new_unit_id\" field)`);
			}
		})

		this.on('close', function(done) {
			client.end();
			done();
		})


	}	// function XkeysSetUnitID

	RED.nodes.registerType("xkeys_setunitid", XkeysSetUnitID);

	RED.httpAdmin.get("/xkeys_setunitid/devices", function (req, res) {
		res.json({"devices":httpAdminDataDevices,"pidOptions":httpAdminDataPidOptions,"quad_list":httpAdminDataQuadList});
		//res.json(httpAdminDataDevices);
	});

	RED.httpAdmin.post("/xkeys_setunit_id_inject/:id", RED.auth.needsPermission("xkeys_setunit_id_inject.write"), function(req,res) {
		//console.log("posting something: " + JSON.stringify(req.body));
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}

