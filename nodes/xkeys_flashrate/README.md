# node-red-contrib-xkeys_flashrate

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices using  the Dynamic Control Data Protocol (DCDP).

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) communicate, using MQTT, with a lightweight [DCDP server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_flashrate_ node configures the LED flashing rate of any _xkeys\_led_ and _xkeys\_backlight_ nodes.


## Installation

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_flashrate_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_flashrate_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk LED rate_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the frequency of LED or backlight flashing for any X-keys device. A setting of 1 produces the fastest flashing while a setting of 255 produces the slowest. In addition, the usual filtering settings to target specific devices by their product_id and/or unit_id are available.

This node is also able to be activated by preceding nodes connected to the input. Such preceding nodes must include, at least, a _flash\_rate_ field in its msg.payload. If the msg.payload also includes optional _pid_list_, _product\_id_, _unit\_id_ or _duplicate\_id_ fields, these will override the _xkeys\_flashrate_ node's configured settings e.g.
```
    {"flash_rate": RATE, "pid_list": PID_LIST, "unit_id": UID}
```
An example flow is provided in the _examples_ directory to demonstrate possible usage of the _xkeys\_flashrate_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_flashrate-test.png" ></p>

The _X-keys LED_ node can be used to activate flashing of all connected device's red led. The rate of flashing may be set by the _xkeys\_flashrate_ node in the upper section, which is configured to set the flash rate to 50 (a somewhat slow rate). However the flashrate may be altered at any time by activating the preceding _Inject_ node (labelled _set flash rate_) which is set to 10 (noteably faster than 50) in this example flow.



## Acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several X-keys devices for development and testing.

## License
MIT
