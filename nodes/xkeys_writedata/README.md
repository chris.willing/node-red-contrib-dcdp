# node-red-contrib-xkeys_writeData

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

It is planned to have a dedicated Node-RED node for each X-key event of interest (button, jog, joystick, etc.). These will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_writeData_ node enables arbitrary command codes to be written to specific X-keys devices.


## Installation

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_writeData_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_writeData_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk Write Data_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

In addition to setting the command code to send to a device, the node's configuration editor must be used to explicitly set the target X-keys Device, as well as the Unit ID of the device. After Node-RED's *Deploy* button is pressed, the node's own built in button can then be used to send the command to the nominated device. A command code may also be provided to the _xkeys\_writedata_ node input from the previous node in a flow. In this case the previous node must include a "commandcode" field in the msg.payload e.g.
```
{"commandcode": "[0,206,0,1,72,101,108,108,111,32,87,111,114,108,100,32,32,32,32,32]"}
```
while the _xkeys\_writedata_ node itself must still be configured with a valid _pid\_list_ (or _pid_) and _unitid_.


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the dcdp-server by running the command `sudo systemctl status dcdp-server` in a terminal. Typical output of a normally running dcdp-server will be:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running dcdp-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the dcdp-server with `sudo systemctl restart dcdp-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support as well as donation of several X-keys devices for development and testing.

## License
MIT
