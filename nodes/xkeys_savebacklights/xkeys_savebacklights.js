
module.exports = function(RED) {
	XKEYS_VENDOR_ID = "1523";
	var mqtt = require('mqtt');
	const connectUrl = 'mqtt://localhost';
	const qos = 0;

	var httpAdminDataDevices = [];
	var httpAdminDataPidOptions = {};
	var httpAdminDataQuadList = [];
	
	function XkeysSaveBacklights(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		node.config = config;
		//node.log(`node.config = ${JSON.stringify(node.config)}`);
		//node.log(`myId = ${node.config.id}`);
		//node.log(`pidOptions = ${JSON.stringify(node.config.pidOptions}}`);

		var client = mqtt.connect(connectUrl);
		client.on('reconnect', (error) => {
			node.log('reconnecting:', error)
		})
		client.on('error', (error) => {
			node.log('Connection failed:', error)
		})
		client.on('connect', () => {
			node.log('connected')
			client.subscribe({'/dcdp/server/#':{qos:qos}}, function (err, granted) {
				if (!err) {
					node.log("Subscribed OK, granted: " + JSON.stringify(granted));
					client.publish('/dcdp/node/xkeys_writedata', '{"msg_type":"list_attached"}');
				} else {
					node.log('Subscription failed: ' + err)
				}
			})
		})
		client.on('close', () => {
			node.log("connection closed");
		})
		client.on('message', (topic, message) => {
			//node.log(`received topic：${topic}, msg: ${message.toString()}`);
			var message_obj = "";
			try {
				message_obj = JSON.parse(message);
				//node.log(`SID = ${message_obj.server_id}`);
				if (message_obj.msg_type == "hello") {
					console.log(`Hello from DCDP server at ${message_obj.sid} - must have just (re)started `);
					// In case dcdp-server restarted with updated devices list
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "attach_event") {
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "detach_event") {
					client.publish('/dcdp/node', JSON.stringify({msg_type:"list_attached"}));
				}
				else if (message_obj.msg_type == "list_attached_result") {
					// data should be an array of device infos
					httpAdminDataDevices = [];

					// Collect only X-keys devices
					message_obj.devices.forEach( (device) => {
						if (device.vendorId == XKEYS_VENDOR_ID) {
							httpAdminDataDevices.push(device);
						}
					});

					/* Check whether a previously configured device has been removed.
					*/
					for (var i=node.config.quad_list.length-1;i>-1;i--) {
						//console.log(`checking ${node.config.quad_list[i]}`);
						if (! httpAdminDataDevices.find( (device) => device.device_quad == node.config.quad_list[i])) {
							// This device was removed so remove it from both
							// node.config.pidOptions and node.config.quad_list
							//
							var index = node.config.pidOptions.findIndex( (opt) => opt.device_quad == node.config.quad_list[i]);
							if (index > -1 ) {
								node.config.pidOptions[index].checked = false;
								node.config.pidOptions.splice(index, 1);
							}
							node.config.quad_list.splice(i,1);
						}
					}
					httpAdminDataPidOptions = node.config.pidOptions;
					httpAdminDataQuadList = node.config.quad_list;

					// Update status (how many configured devices)
					if (node.config.quad_list.length == 0) {
						node.status( {fill:"red",shape:"ring",text:`No devices configured`} );
					}
					else if (node.config.quad_list.length == 1) {
						node.status( {fill:"green",shape:"dot",text:`${node.config.quad_list.length} device configured`} );
					}
					else {
						node.status( {fill:"green",shape:"dot",text:`${node.config.quad_list.length} devices configured`} );
					}
				}
				else if (message_obj.msg_type == "command_result") {
					if (message_obj.command_type == "save_backlight") {
						node.log(`command_result: ${message}`);
					}
				}
				else {
					// Logging here may be useful but is quietened for production
					//node.log(`Received unhandled request: ${message_obj.msg_type}`);
				}
			}
			catch (e) {
				node.log('ERROR parsing message: ' + e);
			}
		})

		/*
	    * Input messages
		* We expect a msg.payloads of
		*
		*/
		node.on('input', function(msg) {
			//node.log("INPUT: " + JSON.stringify(msg));
			var mkeys = Object.keys(msg.payload);
			//console.log("mkeys: " + JSON.stringify(mkeys));
			//console.log("msg.payload: " + JSON.stringify(msg.payload));
			/*
				Check that the request is OK.
			*/
			if (mkeys.includes("quad_list")) {
				if (msg.payload.quad_list.length > 1) {
					node.log(`Only a single target is allowed. Discontinuing ...`);
					return;
				}

			} else {
				node.log(`input command doesn't include target quad_list. Discontinuing ...`);
				return;
			}

			/*  Process incoming requests for particular device(s)
			*
			*	Activation by external nodes is currently disabled.
			*	In case external activation is enabled in the future,
			*	deal with it here. Empty external quad_list => use configured quad_list.
			*/
			var quad_list = [];
			var new_quad_list_needed = false;
			var quad_list_regex_string = XKEYS_VENDOR_ID.toString() + "-";
			if (mkeys.includes("product_id")) {
				new_quad_list_needed = true;
				quad_list_regex_string += msg.payload.product_id + "-";
			} else {
				quad_list_regex_string += "\[0-9\]+-";
			}
			if (mkeys.includes("unit_id")) {
				new_quad_list_needed = true;
				quad_list_regex_string += msg.payload.unit_id + "-";
			} else {
				quad_list_regex_string += "\[0-9\]+-";
			}
			if (mkeys.includes("duplicate_id")) {
				new_quad_list_needed = true;
				quad_list_regex_string += msg.payload.duplicate_id;
			} else {
				quad_list_regex_string += "\[0-9\]+";
			}

			/* 1. If any of product_id, unit_id or duplicate_id was requested,
			*  add any matches to 'quad_list' array.
			*
			*  2. An alternative to adding to the node's default quad_list
			*  would be to restrict to devices already in node's default quad_list.
			*
			*  3. Another alternative would be to match against any attached device.
			*
			*  Here, we're using option 2. Since the config editor restricts the
			*  default quad_list to a single device, the effect is that incoming
			*  device requests must resolve to what is already configured.
			*/
			//if (new_quad_list_needed) {
				// Incoming message requested at least one of product_id, unit_id or duplicate_id
				//console.log(`quad_list_regex_string = ${quad_list_regex_string}`);
				var regex = new RegExp(quad_list_regex_string);
				node.config.quad_list.forEach( (quad) => {
					//console.log(`checking ${quad} against ${quad_list_regex_string}`);
					if (regex.test(quad)) {
						quad_list.push(quad);
					}
				});
				//console.log(`quad_list = ${JSON.stringify(quad_list)}`);
			//}

			/*
			*  If no quad_list is set by anyone, discontinue.
			*/
			if (quad_list.length == 0) {
				if (new_quad_list_needed) {
					// Particular device(s) requested but nothing matched, so discontinue
					node.log(`No devices matched: discontinuing`);
					return;
				} else {
					// Nothing in particular requested so use devices enabled in configuration
					quad_list = node.config.quad_list;
				}
			}
			if (quad_list.length == 0 ) {
				node.log(`No devices enabled: discontinuing`);
				return;
			}

			/*  Prepare output msg but only bother if we have a device with matching device_quad.
			*/
			httpAdminDataDevices.every( (device) => {
				if (quad_list.includes(device.device_quad)) {
					const command_message = {
						msg_type     : "command",
						command_type : "save_backlight",
						vendor_id    : XKEYS_VENDOR_ID,
						product_id   : device.device_quad.split('-')[1],
						unit_id      : device.device_quad.split('-')[2],
						duplicate_id : device.device_quad.split('-')[3]
					}
					//console.log(`command_message = ${JSON.stringify(command_message)}`);
					client.publish('/dcdp/node', JSON.stringify(command_message));

					// Don't break the every loop on first match - find all matches.
					//return false;
				}
				return true;
			});

		})

		this.on('close', function(done) {
			client.end();
			done();
		})

	}	// function XkeysSaveBacklights

	RED.nodes.registerType("xkeys_savebacklights", XkeysSaveBacklights);

	RED.httpAdmin.get("/xkeys_savebacklights/devices", function (req, res) {
		res.json({"devices":httpAdminDataDevices,"pidOptions":httpAdminDataPidOptions,"quad_list":httpAdminDataQuadList});
		//res.json(httpAdminDataDevices);
	});

	RED.httpAdmin.post("/xkeys_savebacklights_inject/:id", RED.auth.needsPermission("xkeys_savebacklights_inject.write"), function(req,res) {
		//console.log("posting something: " + JSON.stringify(req.body));
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				if (req.body) {
					node.receive(req.body);
				} else {
					node.receive();
				}
				res.sendStatus(200);
			}
			catch (err) {
				res.sendStatus(500);
				node.error(RED._("inject.failed",{error:err.toString()}));
			}
		} else {
			console.log("bad post");
			res.sendStatus(404);
		}
	});

}

