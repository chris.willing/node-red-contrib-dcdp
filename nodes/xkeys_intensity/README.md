# node-red-contrib-xkeys_intensity

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices using the Dynamic Control Data Protocol (DCDP).

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight [DCDP server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_intensity_ node configures the maximum intensity to which backlights of attached devices may be set.


## Installation

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_intensity_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_intensity_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk Intensity_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set a value to which the intensity or brightness of any _hue_ set by the _xkeys\_backlight_ node is limited. A setting of 0 produces the minimum intensity while a setting of 255 produces the maximum. In addition, the usual configurable settings to target specific devices by their PID and/or UID are available.

This node is also able to be activated by preceding nodes in a flow connected the node's input. Such preceding nodes must include, at least, an _intensity_ field in its msg.payload. If the msg.payload also includes the optional _pid_list_, _pid_ or _uid_ fields, they override the _xkeys\_intensity_ node's configured settings.
```
    {"intensity": [blue_intensity,red_intensity]}
or
    {"intensity": [blue_intensity,red_intensity], product_id: PID, unit_id: UID}
```
An example flow is provided in the _examples_ directory to demonstrate possible usage of the _xkeys\_intensity_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_intensity-test.png" ></p>

In this example, a _Backlight_ node's builtin toggle switch is used to turn on all backlights of any attached X-keys device. Once it is turned on, two methods are available to now modify the intensity the backlights:
- the first method is to set the intensity by use of the _Intensity_ node's own configuration editor. The new intensity setting takes effect when the node's button is pressed, altering the brightness of all backlights activated by the _Backlight_ node.
- the two _Inject_ nodes demonstrate how other nodes in a flow may control the _Intensity_ node.  When activated, the node labeled _Intensity high_ sets the red & blue intensity values to 255 (the maximum), while the node labelled _Intensity low_ sets both intensity values to 5 (the minimum is 0).

## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several X-keys devices for development and testing.

## License
MIT
