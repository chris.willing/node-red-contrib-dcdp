# node-red-contrib-xkeys_lcd

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices using Dynamic Control Data Protocol (DCDP).

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight [DCDP server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_lcd_ node encapsulates actions for any or specific LCD equiped X-keys devices.


## Installation

This node requires _dcdp-server_ version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_dcdp-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_lcd_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_lcd_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk LCD_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the target Xkeys device, the default line number to write to as well as backlight status.

The msg.payload expected by the _xkey\_lcd_ node is as follows:
```
{ lcdtext: LCDTEXT, linenum: LINENUM, product_id: PID, unit_id: UID, duplicate_id: DUPID }
```
where
- LCDTEXT is the text to display. It may be a simple string, number, or array of the two strings to be displayed.
- LINENUM is 1 or 2, denoting which display line to write to.
- PID is the product Id of the device
- UID is the unit Id of the device
- DUPID is the duplicate Id of the device

Only the _lcdtext_ field is required. The other fields are optional, in which case the default settings (as set in the node configuration editor) for linenum or backlight are used.

An example flow is provided in the examples directory to demonstrate a number of methods to activate the xkeys_lcd node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_lcd-test.png" ></p>

- the first two Inject nodes send "Node-RED" to line 1 and "Hello World!" to line 2 of the display.
- the next Inject node sends text string ("This is the end") as the msg.payloads, which will be displayed on whichever line has been selected in the node configuration (default is line 2).
- the fourth Inject node demonstrates how to set both lines of text in the same message.
- the last three Inject nodes show how to clear all or individual lines of the display.


## Issues

When configuring the node, a drop down list of possible devices should be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the dcdp-server by running the command `sudo systemctl status dcdp-server` in a terminal. Typical output of a normally running dcdp-server will be:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running dcdp-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the dcdp-server with `sudo systemctl restart dcdp-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several X-keys devices for development and testing.

## License
MIT
