# node-red-contrib-xkeys_led

This is another in a collection of Node-RED nodes which enable access to [X-keys](https://xkeys.com/) physical devices.

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight [X-keys server](https://gitlab.com/chris.willing.oss/dcdp-server), whose sole purpose is to mediate access to any physically attached X-keys devices.

This _xkeys\_led_ node encapsulates LED actions for any or specific X-keys devices.


## Installation

This node requires _xkeys-server_ version 0.1.1 to be running. Please follow the instructions at the [_xkeys-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The _node-red-contrib-xkeys\_led_ node itself is best installed from Node-RED's Palette manager. Go to the Palette manager's Install tab and search for _node-red-contrib-xkeys\_led_; then Install it once found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, a new _xk LED_ node will be found in the palette tab in the dedicated _Xkeys_ category.


## Usage

The node's configuration editor can be used to set the target Xkeys device, as well as the target LED (for devices with multiple LEDs). The target LED set in the configuration editor may be overridden by settings the relevant field in the msg.payload of an incoming node. A msg.payload containing only an _action_ field activates the target LED with whatever settings are currently configured in the configuration editor.
The msg.payload expected by the _xkey\_led_ node is as follows:
```
{ action: STARTSTOP, ledid: LEDID, flashing: YESNO }
```
where
- STARTSTOP is either of *"start"* or *"stop"*. This field is required. All other fields are optional and in that case the configured settings of the node are used. An example of such a case is when the node is activated by the attached pushbutton, sending itself just the action field.
- LEDID indicates which the device's LEDs target; 1 targets the green LED, 2 targets the red LED (this is the default). Both LEDs 1 and 2 may be targeted by entering *1,2* in this field.
- YESNO is one of _true_ or _false_

An example flow is provided in the _examples_ directory to demonstrate a number of methods to activate the _xkeys\_led_ node.
<p align="center" width="100%" ><img width="70%" src="examples/xkeys_led-test.png" ></p>

- the first method is the toggle switch attached to the node itself. In the configuration shown, the toggle switch will start/stop the second LED of all attached devices.
- a second method is to use _Inject_ nodes with appropriately crafted msg.payloads. This particular example demonstrates the use of the pid_list field being set to [], a special case indicating that the LEDs of all attached devices should be targeted.
- another method uses an _xkeys\_button_ node to activate the LED node (in this case button 1 for ON, button 3 for OFF). In this configuration, pressing button 1 of a device will start its own LED flashing; pressing button 3 will stop it.

Note the use of a separate _xkeys\_flashrate_ node to set the flashing rate (20 here).


## Issues

When configuring the node, a drop down list of possible devices shoud be available. If not, instead displaying just a note to `Press Deploy button to see device list`, first follow that advice. If that is unsuccessful (still no list of possible devices), then check the status of the dcdp-server by running the command `sudo systemctl status dcdp-server` in a terminal. Typical output of a normally running dcdp-server will be:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: active (running) since Thu 2021-10-14 09:31:55 AEST; 23h ago
```
whereas a non-running dcdp-server will show something like:
```
pi@pi3b:~ $ sudo systemctl status dcdp-server
● dcdp-server.service - Run dcdp-server as background service
Loaded: loaded (/etc/systemd/system/dcdp-server.service; enabled; vendor preset: enabled)
Active: failed (Result: timeout) since Fri 2021-10-15 08:41:37 AEST; 19s ago
```
If necessary, (re)start the dcdp-server with `sudo systemctl restart dcdp-server`


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support as well as donation of several X-keys devices for development and testing.

## License
MIT
