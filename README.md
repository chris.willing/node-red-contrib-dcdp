# node-red-contrib-dcdp

A group of Node-RED nodes to enable access to physical USB devices, in particular [X-keys devices](https://xkeys.com), using the **D**ynamic **C**ontrol **D**ata **P**rotocol (DCDP).

Dedicated Node-RED nodes for each X-key event of interest (button, jog, joystick, etc.) will communicate, using MQTT, with a lightweight DCDP server, whose sole purpose is to mediate access to any physically attached X-keys devices, via the [SuperFlyTv xkeys library](https://github.com/SuperFlyTV/xkeys). The commonly used and recommended [**mosquitto** MQTT server](https://mosquitto.org/), which enables communication with a local DCDP server, can be installed with:
```
    sudo apt install mosquitto
```

## Available xkeys event nodes

The xkeys event nodes available so far are:

- [_xkeys\_button_](nodes/xkeys_button)
- [_xkeys\_jogshuttle_](nodes/jogshuttle)
- [_xkeys\_joystick_](nodes/joystick)
- [_xkeys\_lcd_](nodes/lcd)
- [_xkeys\_rotary_](nodes/rotary)
- [_xkeys\_tbar_](nodes/xkeys_tbar)
- [_xkeys\_trackball_](nodes/xkeys_trackball)

Additional xkeys command nodes available are:
- [_xkeys\_backlight_](nodes/xkeys_backlight)
- [_xkeys\_flashrate_](nodes/xkeys_flashrate)
- [_xkeys\_intensity_](nodes/xkeys_intensity)
- [_xkeys\_led_](nodes/xkeys_led)
- [_xkeys\_save_backlights_](nodes/xkeys_save_backlights)
- [_xkeys\_setunitid_](nodes/xkeys_setunitid)
- [_xkeys\_writedata_](nodes/xkeys_writedata)


## Installation

These nodes require dcdp-server version 0.1.1 to be running. Please follow the instructions at the [_dcdp-server_](https://gitlab.com/chris.willing.oss/dcdp-server) development repository to install it or, to upgrade an existing installation, see the [_xkeys-server_ upgrade](https://gitlab.com/chris.willing.oss/dcdp-server#upgrading) instructions.

The nodes themselves are best installed using Node-RED's Palette manager. Go to the Palette manager's Install tab and search for xkeys_*; then Install nodes when found. If not found, press the _Refresh module list_ button (two semicircular arrows) and search again.

When installed, the nodes will be found in the palette tab in a dedicated _Xkeys_ category.


## Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com) for generous financial support and donation of several X-keys devices for development and testing.

## License
MIT
